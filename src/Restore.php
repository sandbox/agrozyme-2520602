<?php
namespace Drupal\features_installer;

use Drupal\mixin\Traits\Hook;

/**
 * Class PostSestore
 * @package Drupal\features_installer
 */
class Restore {
  use Hook;

  protected $op;
  protected $items;
  protected $component;

  /**
   * Restore constructor.
   *
   * @param array $data
   */
  function __construct(array $data = []) {
    foreach (array_keys(get_object_vars($this)) as $index) {
      if (isset($data[$index])) {
        $this->{$index} = &$data[$index];
      }
    }
  }

  /**
   * @param $op
   * @param $items
   */
  static function hook_features_pre_restore($op, $items) {
    $data = get_defined_vars();
    $item = static::create($data);
    $method = $op . 'Prepare';

    if (is_callable([$item, $method])) {
      $item->{$method}();
    }
  }

  /**
   * @param array $data
   *
   * @return static
   */
  static function create(array $data = []) {
    $item = new static($data);
    return $item;
  }

  /** @return array */
  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['features_pre_restore', 'features_post_restore'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

  /**
   * @param $op
   * @param $items
   */
  static function hook_features_post_restore($op, $items) {
    $data = get_defined_vars();
    $item = static::create($data);
    $method = $op . 'Post';

    if (is_callable([$item, $method])) {
      $item->{$method}();
    }
  }

  function revertPrepare() {
  }

  function rebuildPrepare() {
  }

  function disablePrepare() {
  }

  function enablePrepare() {
  }

  function revertPost() {
  }

  function rebuildPost() {
    module_load_include('inc', 'features', 'features.export');

    foreach (array_keys($this->items) as $module) {
      $this->enableThemes($module);
    }
  }

  /**  @param string $module */
  protected function enableThemes($module) {
    $default = features_get_default('variable_realm', $module);
    $themes = &$default['global']['default']['themekey_ui_selectable_themes'];
    $items = [];

    if (empty($themes)) {
      return;
    }

    foreach (system_rebuild_theme_data() as $index => $item) {
      if ((false == $item->status) && (false == empty($themes[$index]))) {
        $items[] = $index;
      }
    }

    theme_enable($items);
  }

  function disablePost() {
  }

  function enablePost() {
  }

}
