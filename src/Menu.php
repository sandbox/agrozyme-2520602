<?php

namespace Drupal\features_installer;

use Drupal\mixin\Menu as Base;
use Drupal\mixin\Menu\Router;

/**
 * Class Menu
 * @package Drupal\features_installer
 */
class Menu extends Base {
  /** @return array */
  protected function menuAction() {
    $menu = [];
    $base = 'admin/structure/features/features_installer';

    $menu += $this->defaultRouter()
      ->setTitle('Force Disable')
      ->setPageArguments(['features_installer_disable_form'])
      ->export($base);

    //$menu += $this->defaultRouter()->setTitle('View')->export($base . '/view');
    return $menu;
  }

  /** @return Router */
  protected function defaultRouter() {
    return Router::create()
      ->setType(MENU_LOCAL_TASK)
      ->setPageArguments(['drupal_get_form'])
      ->setAccessCallback('user_access')
      ->setAccessArguments(['manage features']);
  }

  //protected function buildActionRouter($title, $name) {
  //  return $this->defaultRouter()
  //    ->setTitle($title)
  //    ->setType(MENU_VISIBLE_IN_BREADCRUMB)->setPageArguments([
  //      'features_installer_' . $name . '_form',
  //      static::POSITION
  //    ])
  //    ->export(Core::$basePath . '/' . $name . '/%feature');
  //}
}
