<?php
namespace Drupal\features_installer;

use Drupal\mixin\Traits\Object;

/**
 * Class Feature
 * @package Drupal\features_installer
 */
class Feature {
  use Object;
  protected $setting = [];

  /**
   * Feature constructor.
   *
   * @param $name
   */
  function __construct($name) {
    $this->setting = features_load_feature($name);
  }

  /** @param array $modules */
  static function setup(array $modules) {
    $modules = array_keys(static::filter($modules));
    features_install_modules($modules);
    static::disable($modules);
  }

  /**
   * @param array $modules
   * @param bool $reset
   *
   * @return array
   */
  protected static function filter(array $modules, $reset = false) {
    $items = [];
    $features = features_get_features(null, $reset);

    foreach ($modules as $module) {
      $feature = &$features[$module];

      if (isset($feature)) {
        $items[$module] = $feature;
      }
    }

    return $items;
  }

  /** @param array $modules */
  static function disable(array $modules) {
    $modules = array_keys(static::filter($modules));
    module_disable($modules, false);
    drupal_uninstall_modules($modules, false);
    node_access_rebuild(true);
  }

}
