<?php
namespace Drupal\features_installer;

use Drupal\mixin\Traits\Hook;

/**
 * Class Pipe
 * @package Drupal\features_installer
 */
class Pipe {
  use Hook;

  /**
   * @param $pipe
   * @param $data
   * @param $export
   */
  static function field_permissions_features_pipe_field_base_alter(&$pipe, $data, $export) {
    module_load_include('inc', 'field_permissions', 'field_permissions.admin');
    $base = &$export['features']['field_base'];

    if (false == isset($base)) {
      return;
    }

    foreach ($base as $field_name) {
      $field = field_info_field($field_name);
      $type = &$field['field_permissions']['type'];

      if ((false == isset($type)) || (FIELD_PERMISSIONS_CUSTOM != $type)) {
        continue;
      }

      foreach (field_permissions_list_field_permissions($field, '') as $perm => $info) {
        $pipe['user_permission'][] = $perm;
      }
    }
  }

  /**
   * @return array
   */
  protected static function getHookMap() {
    $class = get_called_class();
    $hooks = [];
    $items = ['field_permissions_features_pipe_field_base_alter'];

    foreach ($items as $item) {
      $hooks[$class][$item] = $item;
    }

    return $hooks;
  }

}
