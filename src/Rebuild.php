<?php

namespace Drupal\features_installer;

use Drupal\features_installer\Rebuild\Profile2;
use Drupal\mixin\Caller;
use Drupal\mixin\Getter;
use Drupal\mixin\Traits\Hook;
use FeedsPlugin;

/**
 * Class Rebuild
 *
 * @package Drupal\features_installer
 */
class Rebuild {
  use Hook {
    createHooks as createHooksCall;
  }

  /** @param $module */
  static function field_group($module) {
    $callback = function ($index, $item) {
      $load = field_group_load_field_group_by_identifier($item->identifier);

      if (is_object($load) && empty($load->in_code_only)) {
        return;
      }

      $group = field_group_unpack($item);
      field_group_group_save($group);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /**
   * @param $component
   * @param $module
   * @param $callback
   */
  static function save($component, $module, $callback) {
    module_load_include('inc', 'features', 'features.export');
    $default = features_get_default($component, $module);
    //dpm(features_get_components());

    if (empty($default) || (false == is_callable($callback))) {
      return;
    }

    $caller = Caller::create($callback);

    foreach ($default as $index => $item) {
      $caller->__invoke($index, $item);
    }

  }

  /** @param $module */
  static function quicktabs($module) {
    $callback = function ($index, $item) {
      quicktabs_save($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function views_view($module) {
    //dpm(features_get_components());

    $callback = function ($index, $item) {
      views_save_view($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function page_manager_handlers($module) {
    $callback = function ($index, $item) {
      page_manager_save_task_handler($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function password_policy($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      ctools_export_crud_save('password_policy', $item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /**
   * @param $item
   *
   * @return mixed
   */
  static function setExportType($item) {
    if (false == isset($item->export_type)) {
      $item->export_type = EXPORT_IN_CODE;
    }

    return $item;
  }

  /** @param $module */
  static function feeds_importer($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      ctools_export_crud_save('feeds_importer', $item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function feeds_tamper($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      feeds_tamper_save_instance($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function image($module) {
    $callback = function ($index, $item) {
      $ieid = [];
      $effects = [];
      $old = image_style_load($index);

      if (false == empty($style)) {
        $effects = $old['effects'];
        $item['isid'] = $old['isid'];
      }

      $item['name'] = $index;
      $style = image_style_save($item);

      foreach ($item['effects'] as $effect) {
        $effect['isid'] = $style['isid'];
        $old = array_shift($effects);

        if ($old) {
          $effect['ieid'] = $old['ieid'];
        }

        image_effect_save($effect);
      }

      foreach ($effects as $effect) {
        $ieid[] = $effect['ieid'];
      }

      if (false == empty($ieid)) {
        db_delete('image_effects')->condition('ieid', $ieid)->execute();
      }
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function breakpoints($module) {
    $callback = function ($index, $item) {
      $breakpoint = breakpoints_breakpoint_load_by_fullkey($item->machine_name);

      if ($breakpoint && isset($breakpoint->id)) {
        $item->id = $breakpoint->id;
      }

      breakpoints_breakpoint_save($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function breakpoint_group($module) {
    $callback = function ($index, $item) {
      $group = breakpoints_breakpoint_group_load($item->machine_name);

      if ($group && isset($group->id)) {
        $item->id = $group->id;
      }

      breakpoints_breakpoint_group_save($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function picture_mapping($module) {
    $callback = function ($index, $item) {
      picture_mapping_save($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function flexslider_optionset($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      _flexslider_typecast_optionset($item->options);
      ctools_export_crud_save('flexslider_optionset', $item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function flexslider_picture_optionset($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      $name = $item->flexslider_optionset;
      $data =
        ctools_export_load_object('flexslider_picture_optionset', 'conditions', ['flexslider_optionset' => $name]);

      if ((false == empty($data[$name])) && isset($data[$name]->id)) {
        $item->id = $data[$name]->id;
      }

      ctools_export_crud_save('flexslider_picture_optionset', $item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function blockexport_settings($module) {
    blockexport_settings_features_revert($module);
  }

  /** @param $module */
  static function path_breadcrumbs($module) {
    $callback = function ($index, $item) {
      static::setExportType($item);
      ctools_export_crud_save('path_breadcrumbs', $item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  /** @param $module */
  static function dfp_tags($module) {
    $callback = function ($index, $item) {
      dfp_tag_save($item);
    };

    static::save(__FUNCTION__, $module, $callback);
  }

  static function createHooks() {
    Profile2::createHooks();
    static::createHooksCall();
  }

  /** @return array */
  protected static function getHookMap() {
    $class = get_called_class();
    $hooks = [];
    $items = array_keys(static::getComponentModule());

    foreach ($items as $item) {
      $hooks[$class][$item] = $item . '_features_rebuild';
    }

    return $hooks;
  }

  /**
   * @param null $component
   *
   * @return array|mixed
   */
  protected static function getComponentModule($component = null) {
    static $cache;

    if (false == isset($cache)) {
      $modules = [
        'image',
        'breakpoints',
        'breakpoint_group',
        'picture_mapping',
        'flexslider_optionset',
        'flexslider_picture_optionset',
        'field_group',
        'quicktabs',
        'dfp_tags',
        'password_policy',
        'feeds_tamper',
        'path_breadcrumbs',
        'blockexport_settings'
      ];
      $items = array_combine($modules, $modules);
      $items += [
        'page_manager_handlers' => 'page_manager',
        'views_view' => 'view',
        'feeds_importer' => 'feeds',

      ];
      $cache = $items;
    };

    return isset($component) ? Getter::create($cache, [$component], '')->fetch(false) : $cache;
  }

}
