<?php
namespace Drupal\features_installer\Rebuild;

use Drupal\features_installer\Rebuild;
use Drupal\mixin\Traits\Hook;
use ProfileType;

/**
 * Class Profile2
 * @package Drupal\features_installer\Rebuild
 */
class Profile2 {
  use Hook;

  /** @param $components */
  static function profile2_features_api_alter(&$components) {
    $component = &$components['profile2_type'];
    $component['base'] = 'profile2_type';
  }

  /**
   * @param $form
   * @param $form_state
   */
  static function profile2_form_profile2_type_form_alter(&$form, &$form_state) {
    $type = $form['type']['#default_value'];

    if ('' === $type) {
      return;
    }

    $item = profile2_type_load($type);
    $form['data']['roles']['#default_value'] = empty($item->data['roles']) ? [] : $item->data['roles'];
  }

  /**
   * @param $data
   * @param $export
   * @param $module_name
   *
   * @return mixed
   */
  static function profile2_type_features_export($data, &$export, $module_name) {
    return entity_features_export($data, $export, $module_name, 'profile2_type');
  }

  /**
   * @param $a1
   * @param null $a2
   *
   * @return mixed
   */
  static function profile2_type_features_export_options($a1, $a2 = null) {
    return entity_features_export_options($a1, $a2);
  }

  /**
   * @param $module
   * @param $data
   * @param null $export
   *
   * @return mixed
   */
  static function profile2_type_features_export_render($module, $data, $export = null) {
    return entity_features_export_render($module, $data, $export, 'profile2_type');
  }

  /**
   * @param $module
   *
   * @return mixed
   */
  static function profile2_type_features_revert($module) {
    return entity_features_export_revert($module, 'profile2_type');
  }

  /** @param $module */
  static function profile2_type_features_rebuild($module) {
    entity_get_controller('profile2_type')->resetCache();
    $types = profile2_get_types();

    $callback = function ($index, $item) use ($types) {
      $type = $item->type;

      if (isset($types[$type])) {
        $merge = get_object_vars($item) + get_object_vars($types[$type]);
        $merge['is_new'] = empty($merge['id']);
        $item = new ProfileType($merge);
      }

      profile2_type_save($item);
    };

    Rebuild::save('profile2_type', $module, $callback);
  }

  /** @return array */
  protected static function getHookMap() {
    $class = get_called_class();

    $methods = [
      'profile2_features_api_alter',
      //'profile2_form_profile2_type_form_alter',
      'profile2_type_features_export',
      'profile2_type_features_export_options',
      'profile2_type_features_export_render',
      'profile2_type_features_revert',
      'profile2_type_features_rebuild',
    ];

    $items = [];

    foreach ($methods as $item) {
      $items[$class][$item] = $item;
    }

    return $items;
  }

}
