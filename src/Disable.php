<?php
namespace Drupal\features_installer;

use Drupal\mixin\Arrays;
use Drupal\mixin\Form;

/**
 * Class Disable
 * @package Drupal\features_installer
 */
class Disable extends Form {
  protected static $hook = 'disable';
  protected $name = 'tableselect';

  /** @return array */
  protected function doForm() {
    $header = $this->header();
    $options = $this->options();

    $form = parent::doForm();
    $form += $this->formFeature($header, $options);

    if (false == empty($options)) {
      $form += $this->formDisable();
    } else {
      drupal_set_message(t('No enabled features.'));
    }

    return $form;
  }

  /** @return array */
  protected function header() {
    $header = ['feature' => t('Feature')];
    return $header;
  }

  /** @return array */
  protected function options() {
    $options = [];

    foreach (features_get_features(null, true) as $index => $item) {
      if ($item->status) {
        //dpm($item);
        $package = $item->info['package'];
        $options[$package][$index] = [
          'feature' => $item->info['name'],
          'status' => $item->status,
        ];
      }
    }

    ksort($options, SORT_NATURAL | SORT_FLAG_CASE);
    return $options;
  }

  /**
   * @param array $header
   * @param array $options
   *
   * @return array
   */
  protected function formFeature(array $header, array $options) {
    $form = ['tabs' => ['#type' => 'vertical_tabs']];
    $tabs = &$form['tabs'];

    foreach ($options as $index => $item) {
      $tabs[$index] = ['#type' => 'fieldset', '#title' => $index, '#tree' => true];

      $tabs[$index][$this->name] = [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $item,
        '#title' => $index,
      ];
    }

    return $form;
  }

  /** @return array */
  protected function formDisable() {
    $form = [];
    $form['disable'] = [
      '#type' => 'submit',
      '#value' => t('Disable'),
    ];

    return $form;
  }

  /** @return array */
  protected function doSubmit() {
    $items = [];

    $filter = function ($index, $item) {
      return ($index === $item);
    };

    foreach ($this->getFilteredValues() as $item) {
      $items += Arrays::filter($item[$this->name], $filter);
    }

    if (false == empty($items)) {
      Feature::disable($items);
      cache_clear_all('features:features_list', 'cache');
    }
  }

  /** @return array */
  protected function getFilteredValues() {
    $name = $this->name;

    $filter = function ($items) use ($name) {
      return (isset($items[$name]) && is_array($items[$name]));
    };

    return array_filter($this->values, $filter);
  }

}
